# Information / Информация

SPEC-файл для создания RPM-пакета **weechat**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/weechat`.
2. Установить пакет: `dnf install weechat`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)